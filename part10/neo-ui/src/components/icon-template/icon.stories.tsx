import React from "react";
import { Icon } from "./index";
import {
  withKnobs,
  text,
  boolean,
  color,
  select,
} from "@storybook/addon-knobs";

export default {
  title: "Icon",
  component: Icon,
  decorators: [withKnobs],
};

export const knobsBtn = () => (
);