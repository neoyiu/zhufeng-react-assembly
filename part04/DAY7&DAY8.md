今日写下一个组件。Icon

## Icon

icon 这个东西一般要么是拿别人的库，要么是直接复制粘贴。

我的组件库里使用了 fontawesome 的库：https://github.com/yehuozhili/bigbear-ui/blob/master/src/components/Icon/icon.tsx

这个优点就是扩展性好可以 treeShaking 掉多余的 icon。

对于 Icon 数量不要求那么多的情况，也可以自己建个 icon 对象，然后把所要的 icon 复制粘贴进去，这次训练营我们实践这种方法。

### 制作 icon path

首先，在 shared 里建立 icons.tsx

然后复制以下内容 因为内容太长 参考 icon.md

### 组装 icon 组件

使用 plop 新建 icon 组件。

index.tsx:

```typescript
import React from 'react'
import styled from 'styled-components'

import { icons } from '../shared/icons'

const Svg = styled.svg<IconProps>`
  display: ${(props) => (props.block ? 'block' : 'inline-block')};
  vertical-align: middle;

  shape-rendering: inherit;
  transform: translate3d(0, 0, 0);
`

const Path = styled.path`
  fill: ${(props) => props.color};
`

export interface IconProps {
  /** 图标名*/
  icon: keyof typeof icons
  /** 是否块级元素 */
  block?: boolean
  /** 颜色 */
  color?: string
}

export function Icon(props: IconProps) {
  const { block, icon, color } = props
  return (
    <Svg
      viewBox="0 0 1024 1024"
      width="20px"
      height="20px"
      block={block}
      {...props}
    >
      <Path d={icons[icon]} color={color} />
    </Svg>
  )
}
Icon.defaultProps = {
  block: false,
  color: 'black',
}
```

### 编写 story

```typescript
import React from 'react'
import { Icon, IconProps } from './index'
import { withKnobs, color, select } from '@storybook/addon-knobs'
import styled from 'styled-components'
import { icons } from '../shared/icons'

export default {
  title: 'Icon',
  component: Icon,
  decorators: [withKnobs],
}

export const knobsIcon = () => (
  <Icon
    icon={select<IconProps['icon']>(
      'icons',
      Object.keys(icons) as IconProps['icon'][],
      'bookmark'
    )}
    color={color('color', 'black')}
  ></Icon>
)

const Meta = styled.div`
  color: #666;
  font-size: 12px;
`
const List = styled.ul`
  display: flex;
  flex-flow: row wrap;
  list-style: none;
`
const Item = styled.li`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  flex: 0 1 20%;
  min-width: 120px;
  padding: 0px 7.5px 20px;
  svg {
    margin-right: 10px;
    width: 24px;
    height: 24px;
  }
`
export const labels = () => (
  <>
    There are {Object.keys(icons).length} icons
    <List>
      {Object.keys(icons).map((key) => (
        <Item key={key}>
          <Icon icon={key as keyof typeof icons} />
          <Meta>{key}</Meta>
        </Item>
      ))}
    </List>
  </>
)
```

### 编写测试用例

编写测试：

```typescript
import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Icon, IconProps } from '../index'
import { icons } from '../../shared/icons'

function IconTest(icon: IconProps['icon']) {
  const wrapper = render(<Icon icon={icon} data-testid="icon-path"></Icon>)
  const path = wrapper.queryByTestId('icon-path')
  expect(path?.firstChild).toHaveAttribute('d', icons[icon])
  cleanup()
}

describe(' test Icon component', () => {
  it('it should render correct icon ', () => {
    Object.keys(icons).forEach((key) => {
      IconTest(key as IconProps['icon'])
    })
  })
  it('it should render  block ', () => {
    const wrapper = render(
      <Icon icon="mobile" block data-testid="icon-svg"></Icon>
    )
    const svg = wrapper.getByTestId('icon-svg')
    expect(svg).toHaveStyle('display:block')
  })
  it('it should render correct color ', () => {
    let wrapper = render(<Icon icon="mobile" data-testid="icon-path"></Icon>)
    let path = wrapper.queryByTestId('icon-path')
    expect(path).toHaveAttribute('color', 'black')
    cleanup()
    wrapper = render(
      <Icon icon="mobile" color="blue" data-testid="icon-path"></Icon>
    )
    path = wrapper.queryByTestId('icon-path')
    expect(path).toHaveAttribute('color', 'blue')
  })
})
```

## Avatar

### A11y

Accessibility 是可访问性，就是让残障人士也可以使用网站 。

通常这玩意多半会被忽略，但是有些商店上架应用如果过不了他们的 a11y 就会被禁止上架，另外在某些国家没做这些会吃官司。一开始我们装的 a11y 插件就是来检测可访问性的，可以看见 button 属性中没有过检测，因为在纯白背景下，Primary 的取色跟白色对比度不够大，需要加深颜色。这个背景问题我们可以不用管他，具体应该做出实际页面再处理这种颜色对比度的问题。

a11y 还有很多设定，插件不一定检测出来，开发组件时可能需要去设置它。比如 icon 图标可能需要设置 aria-hidden，否则设备可能不明白那玩意是啥。

由于测试可访问性的很多东西都是收费的（比如 webking），目前发现 NVDA 不收费，可以下载用用，windows 自带那个也可以用用：

下载地址https://www.nvaccess.org/download/

用户指南https://github.com/nvaccess/nvda

这个软件怎么玩呢，安装完毕后就可以了，网页里使用上下左右以及 tab 键之类来读屏，这样就能知道开发的组件读屏软件读出来什么样了。有些浏览器不支持这种读屏，比如 360 浏览器，感觉这浏览器直接把这功能给砍了好像，谷歌可以读。

具体怎么写还是要参考下面的网站。

aria 属性值与 role 属性值：https://www.w3.org/TR/wai-aria-1.1/#role_definitions

无障碍这个话题比较大，国内基本不搞，不同设备不同浏览器以及不同读屏软件读同一段网页可能效果都是不一样的，所以训练营不研究这个，主要目的在于让大家了解有这个东西，促进国内无障碍发展。

编写 index.tsx

```typescript
import React, { useMemo } from 'react'
import styled, { css } from 'styled-components'
import { color, typography } from '../shared/styles'
import { glow } from '../shared/animation'
import { Icon } from '../icon'

export const AvatarSize = {
  large: 40,
  medium: 28,
  small: 20,
  tiny: 16,
}

const Image = styled.div<AvatarProps>`
  background: ${(props) => (!props.isLoading ? 'transparent' : color.light)};
  border-radius: 50%;
  display: inline-block;
  vertical-align: top;
  overflow: hidden;
  text-transform: uppercase;

  height: ${AvatarSize.medium}px;
  width: ${AvatarSize.medium}px;
  line-height: ${AvatarSize.medium}px;

  ${(props) =>
    props.size === 'tiny' &&
    css`
      height: ${AvatarSize.tiny}px;
      width: ${AvatarSize.tiny}px;
      line-height: ${AvatarSize.tiny}px;
    `}

  ${(props) =>
    props.size === 'small' &&
    css`
      height: ${AvatarSize.small}px;
      width: ${AvatarSize.small}px;
      line-height: ${AvatarSize.small}px;
    `}

  ${(props) =>
    props.size === 'large' &&
    css`
      height: ${AvatarSize.large}px;
      width: ${AvatarSize.large}px;
      line-height: ${AvatarSize.large}px;
    `}

  ${(props) =>
    !props.src &&
    css`
      background: ${!props.isLoading && '#37D5D3'};
    `}

  img {
    width: 100%;
    height: auto;
    display: block;
  }

  svg {
    position: relative;
    bottom: -2px;
    height: 100%;
    width: 100%;
    vertical-align: top;
  }

  path {
    fill: ${color.medium};
    animation: ${glow} 1.5s ease-in-out infinite;
  }
`

const Initial = styled.div<AvatarProps>`
  color: ${color.lightest};
  text-align: center;

  font-size: ${typography.size.s2}px;
  line-height: ${AvatarSize.medium}px;

  ${(props) =>
    props.size === 'tiny' &&
    css`
      font-size: ${parseFloat(typography.size.s1) - 2}px;
      line-height: ${AvatarSize.tiny}px;
    `}

  ${(props) =>
    props.size === 'small' &&
    css`
      font-size: ${typography.size.s1}px;
      line-height: ${AvatarSize.small}px;
    `}

  ${(props) =>
    props.size === 'large' &&
    css`
      font-size: ${typography.size.s3}px;
      line-height: ${AvatarSize.large}px;
    `}
`

export interface AvatarProps extends HTMLAttributes<HTMLDivElement> {
  /** 是否加载中*/
  isLoading?: boolean
  /** 用户名*/
  username?: string
  /** 图片地址 */
  src?: null | string
  /** 头像大小 */
  size?: keyof typeof AvatarSize
}
interface a11yProps {
  [key: string]: boolean | string
}

export function Avatar(props: AvatarProps) {
  const { isLoading, src, username, size } = props
  const avatarFigure = useMemo(() => {
    let avatarFigure = <Icon icon="useralt" />
    const a11yProps: a11yProps = {}
    if (isLoading) {
      a11yProps['aria-busy'] = true
      a11yProps['aria-label'] = 'Loading avatar ...'
    } else if (src) {
      avatarFigure = <img src={src} alt={username} data-testid="avatar-img" />
    } else {
      a11yProps['aria-label'] = username!
      avatarFigure = (
        <Initial size={size} aria-hidden="true">
          {username!.substring(0, 1)}
        </Initial>
      )
    }
    return avatarFigure
  }, [isLoading, src, username, size])

  return (
    <Image
      size={size}
      isLoading={isLoading}
      src={src}
      {...props}
      data-testid="avatar-div"
    >
      {avatarFigure}
    </Image>
  )
}

Avatar.defaultProps = {
  isLoading: false,
  username: 'loading',
  src: null,
  size: 'medium',
}
```

在 animate 文件添加 glow:

```javascript
import { keyframes } from 'styled-components'
export const glow = keyframes`
  0%, 100% { opacity: 1; }
  50% { opacity: .4; }
`
```

编写 story:

```typescript
import React from 'react'
import { Avatar, AvatarSize } from './index'
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs'

export default {
  title: 'Avatar',
  component: Avatar,
  decorators: [withKnobs],
}
type AvatarSizeType = keyof typeof AvatarSize

export const knobsAvatar = () => (
  <Avatar
    size={select<AvatarSizeType>(
      'size',
      Object.keys(AvatarSize) as AvatarSizeType[],
      'medium'
    )}
    username={text('username', 'yehuozhili')}
    src={text(
      'src',
      'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'
    )}
    isLoading={boolean('isLoading', false)}
  />
)

export const large = () => (
  <div>
    <Avatar isLoading size="large" />
    <Avatar size="large" username="yehuozhili" />
    <Avatar
      size="large"
      username="yehuozhili"
      src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
    />
  </div>
)

export const medium = () => (
  <div>
    <Avatar isLoading />
    <Avatar username="中文" />
    <Avatar
      username="yehuozhili"
      src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
    />
  </div>
)

export const small = () => (
  <div>
    <Avatar isLoading size="small" />
    <Avatar size="small" username="yehuozhili" />
    <Avatar
      size="small"
      username="yehuozhili"
      src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
    />
  </div>
)

export const tiny = () => (
  <div>
    <Avatar isLoading size="tiny" />
    <Avatar size="tiny" username="yehuozhili" />
    <Avatar
      size="tiny"
      username="yehuozhili"
      src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
    />
  </div>
)
```

编写测试：

```typescript
import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { Avatar, AvatarSize } from '../index'

describe('test Avatar component', () => {
  it('it should render default avatar', () => {
    let wrapper = render(<Avatar data-testid="avatar-div"></Avatar>)
    expect(wrapper).toMatchSnapshot()
    let div = wrapper.getByTestId('avatar-div')
    expect(div).toBeInTheDocument()
    let username = wrapper.getByText('l')
    expect(username).toBeTruthy()
  })

  it('it should render correct size', () => {
    let wrapper = render(<Avatar data-testid="avatar-div"></Avatar>)
    let div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle(`height:${AvatarSize.medium}px`)
    expect(div).toHaveStyle(`width:${AvatarSize.medium}px`)
    expect(div).toHaveStyle(`line-height:${AvatarSize.medium}px`)
    let username = div.firstChild
    expect(username).toHaveStyle(`line-height:${AvatarSize.medium}px`)
    cleanup()

    wrapper = render(<Avatar data-testid="avatar-div" size="large"></Avatar>)
    div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle(`height:${AvatarSize.large}px`)
    expect(div).toHaveStyle(`width:${AvatarSize.large}px`)
    expect(div).toHaveStyle(`line-height:${AvatarSize.large}px`)
    username = div.firstChild
    expect(username).toHaveStyle(`line-height:${AvatarSize.large}px`)
    cleanup()

    wrapper = render(<Avatar data-testid="avatar-div" size="small"></Avatar>)
    div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle(`height:${AvatarSize.small}px`)
    expect(div).toHaveStyle(`width:${AvatarSize.small}px`)
    expect(div).toHaveStyle(`line-height:${AvatarSize.small}px`)
    username = div.firstChild
    expect(username).toHaveStyle(`line-height:${AvatarSize.small}px`)
    cleanup()

    wrapper = render(<Avatar data-testid="avatar-div" size="tiny"></Avatar>)
    div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle(`height:${AvatarSize.tiny}px`)
    expect(div).toHaveStyle(`width:${AvatarSize.tiny}px`)
    expect(div).toHaveStyle(`line-height:${AvatarSize.tiny}px`)
    username = div.firstChild
    expect(username).toHaveStyle(`line-height:${AvatarSize.tiny}px`)
    cleanup()

    wrapper = render(<Avatar data-testid="avatar-div" size="medium"></Avatar>)
    div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle(`height:${AvatarSize.medium}px`)
    expect(div).toHaveStyle(`width:${AvatarSize.medium}px`)
    expect(div).toHaveStyle(`line-height:${AvatarSize.medium}px`)
    username = div.firstChild
    expect(username).toHaveStyle(`line-height:${AvatarSize.medium}px`)
  })

  it('should correct loading', () => {
    let wrapper = render(<Avatar data-testid="avatar-div" isLoading></Avatar>)
    expect(wrapper).toMatchSnapshot()
    let div = wrapper.getByTestId('avatar-div')
    let svg = div.firstChild
    expect(svg).toBeVisible()
    cleanup()

    wrapper = render(
      <Avatar
        data-testid="avatar-div"
        isLoading
        username="123"
        src="/"
        size="tiny"
      ></Avatar>
    )
    div = wrapper.getByTestId('avatar-div')
    svg = div.firstChild
    expect(svg).toBeVisible()
  })

  it('should correct img', () => {
    let wrapper = render(
      <Avatar data-testid="avatar-div" src="www.test.com"></Avatar>
    )
    let div = wrapper.getByTestId('avatar-div')
    let img = div.firstChild
    expect(img.tagName).toEqual('IMG')
    expect(img).toHaveStyle('width:100%')
    expect(img).toHaveAttribute('src', 'www.test.com')
    expect(img).toHaveAttribute('alt', 'loading')
    cleanup()

    wrapper = render(
      <Avatar
        data-testid="avatar-div"
        src="www.yehuozhili.xyz"
        username="yehuozhili"
      ></Avatar>
    )
    div = wrapper.getByTestId('avatar-div')
    img = div.firstChild
    expect(img).toHaveAttribute('src', 'www.yehuozhili.xyz')
    expect(img).toHaveAttribute('alt', 'yehuozhili')
  })

  it('should render correct username', () => {
    let wrapper = render(
      <Avatar data-testid="avatar-div" username="yehuozhili"></Avatar>
    )
    expect(wrapper).toMatchSnapshot()
    let div = wrapper.getByTestId('avatar-div')
    expect(div).toHaveStyle('text-transform:uppercase')
    let username = wrapper.getByText('y')
    expect(username).toBeVisible()
    cleanup()

    wrapper = render(<Avatar username="中文汉字"></Avatar>)
    username = wrapper.getByText('中')
    expect(username).toBeTruthy()
  })
})
```

## 今日作业

完成 icon 与 avatar

### 可选作业

#### 使用 NP

np 是一个标准化发布工具，避免手动更改 version 发布手动打 tag 以及手动写 changelog 等流程，发布前还会进行检测 git 是否有未提交代码。

官网地址https://www.npmjs.com/package/np

我们进行安装 np，使用-D 安装或者全局安装，提交所有代码。

np 本身是不支持 githubpackage 包的发布的，所以我们还需要修改其源码，找到 np 下的 source\prerequisite-tasks.js

将第 32 行 verify user is authenticated 到第 54 行给注释掉。这样就可以支持 githubpackage 包的发布了。

喜欢折腾的可以试试用 patch-package 或者别名搞一下。

#### 使用 PLOP

我们还需要写下一个组件，但是很多东西实际上是重复的代码。所以需要 PLOP 来帮助我们完成这个工作。

官网地址：https://plopjs.com/documentation/

安装完毕后建立 plopfile.js

```javascript
module.exports = function (plop) {
  // controller generator
  plop.addHelper('headCaps', function (p) {
    p = p.trim()
    return p.slice(0, 1).toUpperCase() + p.slice(1)
  })
  plop.setGenerator('component', {
    description: 'create component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'component name please',
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/components/{{name}}/index.tsx',
        templateFile: 'plop-template/componentIndex.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/__test__/{{name}}.test.tsx',
        templateFile: 'plop-template/componentTest.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{name}}/{{name}}.stories.tsx',
        templateFile: 'plop-template/componentStories.hbs',
      },
    ],
  })
}
```

新建 plop-template 制作 hbs 文件模板：

index 模板：

```
import React, {
	PropsWithChildren,
	ReactNode,
	useMemo,
} from "react";
import styled from "styled-components";
import { color, typography } from "../shared/styles";
import { darken, rgba, opacify } from "polished";
import { easing } from "../shared/animation";


type {{headCaps name}}Props = {}

export function {{headCaps name}}(props: PropsWithChildren< {{headCaps name}}Props>) {
	const { children } = props;
	return <div ></div>

}
```

story 模板：

```
import React from "react";
import { {{headCaps name}} } from "./index";
import {
	withKnobs,
	text,
	boolean,
	color,
	select,
} from "@storybook/addon-knobs";

export default {
	title: "{{headCaps name}}",
	component: {{headCaps name}},
	decorators: [withKnobs],
};

export const knobsBtn = () => (
);
```

test 模板：

```
import React from "react";
import { render, fireEvent, cleanup } from "@testing-library/react";
import { {{headCaps name}} } from "../index";
import { color, typography } from "../../shared/styles";




describe("test {{headCaps name}} component", () => {
    it(" ", () => {})
})
```

最后运行命令提示符，使用 npx plop 输入 Icon，就完成下一个组件的初始化了。
