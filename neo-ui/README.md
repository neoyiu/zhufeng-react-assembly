"build-modern": "microbundle build --tsconfig tsconfig.build.json --jsx React.createElement --format modern",

报错(terser plugin) SyntaxError: Unexpected token: punc (.)

要改成
    "build-modern": "microbundle build --tsconfig tsconfig.build.json --jsx React.createElement --no-compress --format modern",
才不报错

{
  "main": "dist/foo.js",            // CommonJS bundle "build-cjs"
  "umd:main": "dist/foo.umd.js",    // UMD bundle "build-umd"
  "module": "dist/foo.m.js",        // ES Modules bundle "build-es"
  "esmodule": "dist/foo.modern.js", // Modern bundle "build-modern"
  "types": "dist/foo.d.ts"          // TypeScript typings directory
}