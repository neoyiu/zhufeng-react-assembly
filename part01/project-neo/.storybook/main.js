module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/preset-create-react-app",
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-viewport",
    "@storybook/addon-knobs",
    {name: "@storybook/addon-docs", options: { configureJSX: true}},
    "@storybook/addon-a11y",
    "@storybook/addon-storysource",
    "@storybook/addon-essentials"
  ],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [{
        loader: require.resolve("react-docgen-typescript-loader")
      }]
    })
    config.resolve.extensions.push(".ts", ".tsx")
    return config
  }
}